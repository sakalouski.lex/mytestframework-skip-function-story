import importlib.util
import sys
import traceback

class MyTestFramework:
    default_test_prefix = "mytest_"
    
    def __init__(self, file_path: str, test_prefix: str = ""):
        self.file_path = file_path
        self.test_prefix = test_prefix or self.default_test_prefix
        self.last_result = {}

    def run_tests(self):
        results = {}
        try:
            spec = importlib.util.spec_from_file_location("module", self.file_path)
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)
            for name in dir(module):
                obj = getattr(module, name)
                if callable(obj) and name.startswith("mytest_"):
                    if getattr(obj, "_mytest_skip", False):
                        results[name] = {"result": "skipped", "status": "skipped"}
                        continue
                    
                    try:
                        result = obj()
                        results[name] = {"result": result, "status": "passed"}
                    except Exception as e:
                        results[name] = {"error": traceback.format_exc(), "status": "failed"}
        except Exception as e:
            results["error"] = traceback.format_exc()

        self.last_result = result
        return results
    
    def print_result(self, test_results=None):
        if test_results is None:
            test_results = self.last_result
            
        for test_name, result in test_results.items():
            print(f"Test: {test_name}")
            if "result" in result:
                print(f"Result: {result['result']}")
            elif "error" in result:
                print(f"Error: {result['error']}")
            print("=" * 50)
            
def skip(func):
    func._mytest_skip = True
    return func


# Example usage: python mytest.py test.py
if __name__ == "__main__":
    
    if len(sys.argv) != 2:
        print("Usage: python my_test_framework.py <file_path>")
        sys.exit(1)

    file_path = sys.argv[1]
    mtf = MyTestFramework(file_path)
    test_results = mtf.run_tests()
    mtf.print_result(test_results)
