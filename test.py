import math

from mytest import skip

def test__not_run_this():
    assert 2 * 2 == 5, "You're not a Jewish accountant so that's not equal"

def mytest_usual_mult_math():
    x = 2 * 2
    
    assert x == 4, "Oops, something goes wrong in this world!"

@skip
def mytest_wartime_sin_math():
    sin = math.sin(130)

    assert sin == 2, "Even in wartime, sin still can't equal 2"

@skip
def mytest_wartime_cos_math():
    cos = math.cos(130)

    assert cos != 2, "Even in wartime, cos still can't equal 2"

def mytest_magic_python():
    s = "*" * 8 + "=)"
    
    assert s == "********=)", "Don't forget to smile!"
